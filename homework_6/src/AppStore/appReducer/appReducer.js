import {combineReducers} from "redux"
import {cardsReducer} from "./cardReducer"
import {cartsReducer} from "./cartReducer"
import {modalReducer} from "./modalReducer"

export const appReducers = combineReducers({
  cards: cardsReducer,
  carts: cartsReducer,
  modal: modalReducer,
})