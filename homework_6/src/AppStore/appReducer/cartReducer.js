import { ADD_CART, DELETE_CARTS, GET_DATA_TO_CART, CLEANER_CART } from "../actions/actions"

const initialState = {
  carts:[],
}
console.log(initialState);

export const cartsReducer = (state = initialState, action)=> {
  if (action.type === ADD_CART) {
    const index = state.carts.findIndex(({name}) => {
      return action.payload === name
    })
    console.log(index);
    if (index === -1) {
      return {...state, carts: [...state.carts, {name: action.payload, count: 1}]}
    } else {
      const newState = [...state.carts]
      newState[index].count = newState[index].count + 1
      console.log(newState);
      return {...state, carts: [...newState]}
    }
  } else if (action.type === DELETE_CARTS) {
    const index = state.carts.findIndex(({name}) => {
      return action.payload === name
    })
    const newState = [...state.carts];
    console.log(newState)
    if (newState[index].count === 1) {
      console.log(newState[index])
      newState.splice(index, 1)
    } else {
      newState[index].count = newState[index].count - 1;
    }

    return {...state, carts: [...newState]}

  } else if (action.type === GET_DATA_TO_CART) {
    return {...state, carts: [...action.payload]}
  }else if(action.type === CLEANER_CART){
    return {
      ...state,
      carts: action.payload,
	  
    }


  }
  
  else{
    return state
  }
}