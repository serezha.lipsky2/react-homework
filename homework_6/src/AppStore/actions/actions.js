export const GET_ITEMS = 'GET_ITEMS';
export const SET_CARDS = 'SET_CARDS';

export const SET_IS_OPEN = 'SET_IS_OPEN';
export const CLEANER_CART = 'CLEANER_CART';
export const TOGGLE_FAV = 'TOGGLE_FAV';
export const ADD_CART = 'ADD_CART';
export const DELETE_CARTS = 'DELETE_CARTS';
export const GET_DATA_TO_CART = 'GET_DATA_TO_CART';
export const GET_DATA_TO_FAV = 'GET_DATA_TO_FAV';