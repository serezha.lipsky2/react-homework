
import React from "react";
import s from "./FavoritePage.module.scss";


function FavoritePage(props){
const{ favorite} = props
console.log(favorite);
	return(
		<>
		<div className={s.fav}>
			<div className={s.container}>
				<h2 className={s.favName}>Избранное</h2>
				<div className={s.favItem}>
					{favorite.map(elem=>(
						<div key={elem.name} className={s.element}>
							<p>{elem.payload}</p>
							
						</div>
					))}
					
				</div>
			</div>
		</div>
		</>
	)
}
export default FavoritePage