import React from "react";
import s from './Cards.module.scss'
import Favorite from "../Favorite/Favorite";
import PropTypes from "prop-types";

function Cards (props) {
	
		const {cards,delFavorite, addFavorite, openModal, setIsAddModal} = props
		console.log(cards);
  return (
	  <>
		<div className = {s.container}>
			<div className = {s.card}>
	  {cards.map(elem =>(<div key = {elem.name} {...elem} className={s.cardsForm}>
			  <div>
				  <img className={s.cardsImg} src={elem.url} alt="icon" />
			  </div>
			  <div className= {s.contant}>
				  	<Favorite {...elem}  addFavorite={addFavorite} delFavorite={delFavorite}/>
			  		<h3>{elem.name}</h3>
					<h2>{elem.price} ₴</h2>
					<p>Код: {elem.code}</p>
					<p>{elem.color}</p>
			  </div>
			  <div className={s.addCartBtn}>
			  <button disableElevation  onClick={()=>{
				  setIsAddModal(true)
				  openModal(elem.name)}}  >Добавить в корзину</button>
			  </div>
	</div>))}
	</div>
		</div>
	  </>
  )
}
Cards.propTypes={
	cards:PropTypes.array.isRequired,
	addFavorite:PropTypes.func.isRequired,
	addCarts:PropTypes.func.isRequired,
	openModal:PropTypes.func.isRequired,
}
export default Cards