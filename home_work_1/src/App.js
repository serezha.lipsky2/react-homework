import './App.scss';
import React from 'react';
import Modal from "./componets/Modal/Modal"

class App extends React.Component{
  state={
    firstModal:false,
    secondModal:false,
  }

  firstModalClose = ()=>{
    this.setState({
      firstModal:false,
    })
  }

  firstModalOpen = ()=>{
    this.setState({
      firstModal:true,
    })
  }

  secondModalClose= ()=>{
    this.setState({
      secondModal:false,
    })
  }

  secondModalOpen = ()=>{
    this.setState({
      secondModal:true,
    })
  }
 
render() {
  return(
    <div className="app">
      {window.addEventListener("click", ()=>{
        this.firstModalClose()
        this.secondModalClose()
      })}
    <header>
     <div className="container">
       <section onClick={event => event.stopPropagation()}>
        <button className= "openModel" onClick={this.firstModalOpen}>Open first modal</button>
        {this.state.firstModal && <Modal
          header='Do you want to delete this file?'
          text='Once you delete this file, it won’t be possible to undo this action.
             Are you sure you want to delete it?'
          textColor={{color: '#fff'}}
          closeBtn={this.firstModalClose}
          background={{backgroundImage: 'linear-gradient(90deg, rgba(242,164,16,1) 14%, rgba(199,35,20,0.949550682089242) 45%)'}}
          headerBg={{backgroundColor: '#c72314'}}
          colorBtn={{backgroundColor: '#b3382c'}}
          firstBtn='Ok'
          secondBtn='Cancel'/>
         }
        <button className= "openModel" onClick={this.secondModalOpen}>Open second modal</button>
        {this.state.secondModal && <Modal
          header='Iron Man'
          text='The event series is owned by The Ironman Group, which is owned by Advance Publications, following the acquisition from the Wanda Sports Group in August 2020'
          textColor={{color: '#fff'}}
          closeBtn={this.secondModalClose}
          background={{backgroundImage: 'linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(9,9,121,1) 35%, rgba(0,212,255,1) 100%)'}}
          headerBg={{backgroundColor: '#090979'}}
          colorBtn={{backgroundColor: '#020024'}}
          firstBtn='Ok'
          secondBtn='Cancel'/>
         }
       </section>

     </div>
    </header>
  </div>
  );
}
}

export default App;
