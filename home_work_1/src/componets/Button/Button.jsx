import React from "react";
import s from './Button.module.scss';

class Button extends React.Component{
    render(){
        const {backgroundColor, text}= this.props
        return(
            <button style={backgroundColor} className={s.btn}>{text}</button>
        )
    }
}

export default Button