import React from "react";
import style from './Modal.module.scss'
import closeImg from './img/cross.png'
import Button from "../Button/Button"

class Modal extends React.Component{
    render(){
        const {header, background, colorBtn, textColor, headerBg, closeBtn, text, firstBtn, secondBtn }= this.props

        return(
            <>
            <form style={background}>
<header style={headerBg}>
<h2>{header}</h2>
<img className={style.closes} src={closeImg} alt="Close Button" onClick={closeBtn}></img>
</header>
<main>
<p style={textColor}>{text}</p>
</main>
<footer>
    <Button backgroundColor={colorBtn} text={firstBtn}/>
    <Button backgroundColor={colorBtn} text={secondBtn}/>
</footer>
            </form>
            </>
        );
    }
}

export default Modal