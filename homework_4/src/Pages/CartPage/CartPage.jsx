import React from "react";
import s from './CartPage.module.scss'
import { IconButton} from "@mui/material";
import DeleteIcon from '@mui/icons-material/Delete';

function CartPage(props){
 const {carts, current, openModal, setIsAddModal} = props

	return(
		<>
		<div className={s.cart}>
			<div className={s.container}>
				<h2 className={s.cartName}>Корзина</h2>
				<div className={s.cartsItem}>
					{carts.map(elem =>(
					<div key={elem.name} className={s.element}>
						 <div className={s.closeWrapper}>
						<IconButton onClick={()=>{
							setIsAddModal(false)
							openModal(current)
							}} aria-label="delete" size="small"><DeleteIcon fontSize="inherit"/></IconButton>
						</div>
						<p> Название: {elem.name} <br /> <span>кол-во: {elem.count}</span></p>
					</div>))}
					
				</div>
			</div>
		</div>
		</>
	)
}
export default CartPage