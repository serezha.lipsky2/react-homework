import React from "react";
import Main from "../../components/Main/Main";
function HomePage(props){
	const {openModal, delFavorite, addFavorite, addCarts, cards, modal, setIsAddModal} = props


	return(
		<div>
			<Main setIsAddModal={setIsAddModal} delFavorite={delFavorite} openModal={openModal} modal = {modal}  cards = {cards} addCarts={addCarts} addFavorite={addFavorite}/>
		</div>
	)
}
export default HomePage