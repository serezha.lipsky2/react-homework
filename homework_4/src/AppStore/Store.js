import thunk from "redux-thunk"
import {createStore, applyMiddleware} from "redux";
import {composeWithDevTools} from "redux-devtools-extension";
import {appReducers} from "./appReducer/appReducer"

export const store = createStore(appReducers, composeWithDevTools(applyMiddleware(thunk)))