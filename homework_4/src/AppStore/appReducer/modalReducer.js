import { SET_IS_OPEN } from "../actions/actions";

const initialState = {
  isOpen: false
}

export const modalReducer = (state = initialState, action)=>{
  switch (action.type){
    case SET_IS_OPEN:{
      return {...state, isOpen: action.payload}
    }
    default:
      return state
  }
}
