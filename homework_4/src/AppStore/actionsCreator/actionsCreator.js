import {
	GET_ITEMS,
	SET_IS_OPEN,
	ADD_CART,
	TOGGLE_FAV,
	DELETE_CARTS,
	GET_DATA_TO_CART,
	GET_DATA_TO_FAV,
  } from '../actions/actions'

  export const getData = () => async (dispatch) => {
	const data = await fetch('collection.json').then(res => res.json())
  console.log(data);
	dispatch({type: GET_ITEMS, payload: data})
  }
  export const setCards = arr => (dispatch) => dispatch({type: GET_ITEMS, payload: arr})
  
  export const setOpen = isOpen => ({type: SET_IS_OPEN, payload: isOpen})
  
  export const addCarts = (name) => ({type: ADD_CART, payload: name})
  export const addFavorite = (name) => ({type: TOGGLE_FAV, payload: name})
  export const getDataToFav = array => ({type: GET_DATA_TO_FAV, payload: array})
  export const deleteCarts = name => ({type: DELETE_CARTS, payload: name})
  export const getDataToCart = array => ({type: GET_DATA_TO_CART, payload: array})