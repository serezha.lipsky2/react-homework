import React from "react";
import {Switch, Route, Redirect} from "react-router-dom";
import HomePage from "../Pages/HomePage/HomePage";
import CartPage from "../Pages/CartPage/CartPage";
import FavoritePage from "../Pages/FavoritePage/FavoritePage"
function Routes(props){
	const {openModal, addFavorite, addCarts, cards, modal, carts, deleteCarts, current, favorite, delFavorite, setIsAddModal} = props



	return(
		<Switch>
			<Route exact path="/home">
				<HomePage setIsAddModal={setIsAddModal} delFavorite={delFavorite} openModal={openModal} modal = {modal}  cards = {cards} addCarts={addCarts} addFavorite={addFavorite} current={current}/>
			</Route>
			<Route exact path="/">
                <Redirect to='/home' />
            </Route>
			<Route exact path="/cart">
				<CartPage carts={carts} deleteCarts={deleteCarts} current={current} openModal={openModal} setIsAddModal={setIsAddModal}/>
			</Route>
			<Route exact path="/favorite">
				<FavoritePage delFavorite={delFavorite} cards = {cards} current={current} favorite={favorite} addFavorite={addFavorite}/>
			</Route>
		</Switch>
	)
}
    

  export default Routes