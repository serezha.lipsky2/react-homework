import React, { Component } from "react";
import s from './Modal.module.scss'
import { Button } from "@mui/material";
import PropTypes from "prop-types";

class Modal extends Component{

    render(){
const{closeModal, addCarts, name, modal}= this.props;

    if (!modal) return null;

        return (
             <div className={s.root}>
                 <div className={s.background} />
                 <div className={s.content}>
                     <div className={s.closeWrapper}>
                         <Button onClick={closeModal} variant="text" color="warning">Close</Button>
                     </div>
					 <div className= {s.success}>
                     <h2>Добавить в вашу корзину?<br />{name}</h2>
					 </div>
                     <div className={s.buttonContainer}>
						 <Button onClick={()=>{addCarts(name)}} variant="outlined" color= "success">Add</Button>
					 </div>
                 </div>
             </div>
        );
    }
};
Modal.propTypes ={
	closeModal:PropTypes.func.isRequired,
	addCarts:PropTypes.func.isRequired,
	modal:PropTypes.bool.isRequired,
	
}
export default Modal;
