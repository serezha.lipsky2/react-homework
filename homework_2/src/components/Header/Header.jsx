import React, { Component } from "react";
import s from './Header.module.scss'

class Header extends Component{

	render(){
		return(
			<header className = {s.header}>
				<div className = {s.container}>
					<div className= {s.logo}>
						<h2 className = {s.logoName}>Comp-<span>Tech</span></h2>
					</div>
				</div>
			</header>
		)
	}
}

export default Header;