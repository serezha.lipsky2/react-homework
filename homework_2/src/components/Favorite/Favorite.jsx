import React, { Component } from "react";
import SvgIcon from '@mui/material/SvgIcon';
import PropTypes from "prop-types";
function HomeIcon(props) {
	return (
	  <SvgIcon {...props}>
		<path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z" />
	  </SvgIcon>
	);
  }

class Favorite extends Component{
	render(){
		const {isFavorite, name, addFavorite} = this.props
		return(
			<div>
				{!isFavorite ? <HomeIcon color="action" style= {{cursor: 'pointer'}} onClick={()=>{addFavorite(name)}} />
        :
        <HomeIcon color="success" style= {{cursor: 'pointer'}} onClick={()=>{addFavorite(name)}} />}
			</div>
		)
	}
}
Favorite.propTypes={
	isFavorite:PropTypes.bool.isRequired,
	addFavorite: PropTypes.func.isRequired,
	name: PropTypes.string.isRequired,
}
export default Favorite