import React from "react";
import s from './Cards.module.scss'
import { Button } from "@mui/material";
import Favorite from "../Favorite/Favorite";
import PropTypes from "prop-types";
class Cards extends React.Component {
	render(){
		const {cards, addFavorite, addCarts, openModal} = this.props
		console.log(cards);
  return (
	  <>
		<div className = {s.container}>
			<div className = {s.card}>
	  {cards.map(elem =>(<div key = {elem.name} className={s.cardsForm}>
			  <div>
				  
				  <img className={s.cardsImg} src={elem.url} alt="icon" />
			  </div>
			  <div className= {s.contant}>
				  	<Favorite {...elem} addCarts={addCarts} addFavorite={addFavorite}/>
			  		<h3>{elem.name}</h3>
					<h2>{elem.price} ₴</h2>
					<p>Код: {elem.code}</p>
					<p>{elem.color}</p>
			  </div>
			  <div className={s.addCartBtn}>
			  <Button variant="outlined" color="success"  onClick={()=>{openModal(elem.name)}}  >Добавить в корзину</Button>
			  </div>
	</div>))}
	</div>
		</div>
	  </>
  )
}}
Cards.propTypes={
	cards:PropTypes.array.isRequired,
	addFavorite:PropTypes.func.isRequired,
	addCarts:PropTypes.func.isRequired,
	openModal:PropTypes.func.isRequired,
}
export default Cards