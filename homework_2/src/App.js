import React from "react";
import Header from "./components/Header/Header";
import Main from "./components/Main/Main";
import Modal from "./components/Modal/Modal";
import Cart from "./components/Cart/Cart";


class App extends React.Component {
	state= {
		cards: [],
		carts: [],
		modal: false,
		currentCards: {},
	}

	openModal=(elem)=>{
		this.setState((current) => {
			return {
			  ...current,
			  currentCards: elem
			}
		  })
		this.setState({modal: true})
	}
	closeModal = () => {
		this.setState({modal: false})
	  }

	 componentDidUpdate() {
    window.localStorage.setItem('carts', JSON.stringify(this.state))
  }

	async componentDidMount(){
		try {
			const state = window.localStorage.getItem('carts');
			this.setState({...JSON.parse(state)})
		  } catch (err) {
			console.log(err)
		  }

		  try {
			const res = await fetch('./collection.json')
			 .then(elem => elem.json())
			console.log(res)
	  
			this.setState({
			  cards: res,
			})
		  } catch (error) {
			console.log(error)
		  }
	  
		}
		

	addCarts = (element) => {
		const carts = this.state.carts
		const index = carts.findIndex(({name: arrayName}) => {
		  return element === arrayName
		})
		if (index === -1) {
		  this.setState((current) => ({
			...current,
			carts: [
			  ...current.carts,
			  {
				element,
				count: 1
			  }
			]
		  }))
		} else {
		  carts[index].count++
		  this.setState((state) => {
			return {
			  ...state,
			  carts: carts
			}
		  })
		}
	  }

	  addFavorite = (name) =>{
		  const card = this.state.cards
		  const index = card.findIndex(({name: arrayName}) => {
			return name === arrayName;
		  })
		  const cards = [...card];
		  if(cards[index].isFavorite === false){
			  cards[index].isFavorite = true
			  this.setState((current)=>{
				  return{
					  ...current,
					  cards:cards
				  }
			  })
		  }
		  else{
			  cards[index].isFavorite = false
			  this.setState((state)=>{
				 return{
					 ...state,
					 cards:cards
				 }
			  })
		  }
	  
	  }
	

	render(){
		const{cards, modal, currentCards, carts}= this.state;
		console.log(carts);
		return(
			<div className="App">
				<header>
					<Header/>
					<Cart carts={carts}/>
				</header>
				<main>
					
				<Main openModal={this.openModal} modal = {modal}  cards = {cards} addCarts={this.addCarts} addFavorite={this.addFavorite}/>
				<Modal closeModal={this.closeModal} modal = {modal} addCarts={this.addCarts} name={currentCards}/>
				</main>
				
			</div>
		)
	}
}
export default App;
