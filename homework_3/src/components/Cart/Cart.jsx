import React from 'react';
import s from './Cart.module.scss';
import iconCart from './icon/iconCart.png'
import PropTypes from "prop-types";
function Cart (props) {
	
		const {carts} = props
		return(
		 <section className={s.cartWrapper}>
		   <div className={s.container}>
		   <div className={s.cartItem}>
			 <div>
			   <img src={iconCart} alt="icon"/>
			 </div>
			 <p>В корзине <span>{carts.length}</span></p>
		   </div>
		   </div>
		 </section>
		);
	  
	
}
Cart.propTypes ={
	carts:PropTypes.array.isRequired,
}
export default Cart;
