import React from "react";
import Cards from "../Cards/Cards";
import s from './Main.module.scss';
import PropTypes from "prop-types";

function Main (props){

	
		const {openModal,delFavorite, addFavorite, addCarts, cards, current, setIsAddModal} = props
		return(
		<main className = {s.main}>
			<div className = {s.container}>
				<div className = {s.mainRoot}>
					<Cards setIsAddModal={setIsAddModal} delFavorite={delFavorite} openModal ={openModal} cards = {cards} addCarts={addCarts} addFavorite={addFavorite} current={current}/>
					
				</div>
			</div>
		</main>
		)
	
}
Main.propTypes={
	openModal:PropTypes.func.isRequired,
	cards:PropTypes.array.isRequired,
	addFavorite:PropTypes.func.isRequired,
	addCarts:PropTypes.func.isRequired,
}
export default Main;