import React from "react";
import s from './Modal.module.scss'
import { Button } from "@mui/material";
import PropTypes from "prop-types";

function Modal (props){

    
const{ isAddModal, closeModal, modal, current, addCarts, deleteCarts}= props;
    if (!modal) return null;
	
	console.log(isAddModal);
        return (
             <div className={s.root}>
                 <div className={s.background} />
                 <div className={s.content}>
                     <div className={s.closeWrapper}>
                         <Button onClick={closeModal} variant="text" color="warning">Закрыть</Button>
                     </div>
					 <div className= {s.main}>
                     <h2>{current}</h2>
					 </div>
                     <div className={s.buttonContainer}>
					{isAddModal? <Button onClick={()=>{
						addCarts(current)
						closeModal()}} variant="outlined" color= "success">Добавить</Button>:
						<Button onClick={()=>{deleteCarts(current)
							closeModal()}} variant="outlined" color= "success">Удалить</Button>}
					 </div>
                 </div>
             </div>
        );
    
};
Modal.propTypes ={
	closeModal:PropTypes.func.isRequired,
	addCarts:PropTypes.func.isRequired,
	modal:PropTypes.bool.isRequired,
	
}
export default Modal;
