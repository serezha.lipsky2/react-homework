
import React from "react";
import s from "./FavoritePage.module.scss"
import { IconButton} from "@mui/material";
import DeleteIcon from '@mui/icons-material/Delete';


function FavoritePage(props){
const{ favorite, delFavorite, current} = props
console.log(current);
	return(
		<>
		<div className={s.fav}>
			<div className={s.container}>
				<h2 className={s.favName}>Избранное</h2>
				<div className={s.favItem}>
					{favorite.map(elem=>(
						<div key={elem.name} className={s.element}>
							 <div className={s.closeWrapper}>
								<IconButton onClick={(()=>{delFavorite(current)})}  aria-label="delete" size="small"><DeleteIcon fontSize="inherit"/></IconButton>
							</div>
							<p>{elem.name}</p>
							
						</div>
					))}
					
				</div>
			</div>
		</div>
		</>
	)
}
export default FavoritePage