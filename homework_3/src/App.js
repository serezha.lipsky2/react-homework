import React, { useEffect, useState } from "react";
import Header from "./components/Header/Header";
import Modal from "./components/Modal/Modal";
import Cart from "./components/Cart/Cart";
import {BrowserRouter as Router} from "react-router-dom";
import Routes from './Routes/Routes'



function App () {
	
	const [cards, setCards] = useState(() => {
		const saved = localStorage.getItem('cards');
		const initialState = JSON.parse(saved);
		return initialState || [];
	  });
	  
	  const [carts, setCarts] = useState(() => {
		const saved = localStorage.getItem('carts');
		const initialState = JSON.parse(saved);
		return initialState || [];
	  });

	const[current, setCurrent] = useState('')
	const[modal, setModal] = useState(false)

	const [favorite, setFavorite] = useState(() => {
		const saved = localStorage.getItem('favorite');
		const initialState = JSON.parse(saved);
		return initialState || [];
	  });

	const[isAddModal, setIsAddModal]=useState(true)

	const openModal = (name) =>{
		
		setModal(true)
		setCurrent(name)
	};
	const closeModal = () => {
		setModal(false)
	  };


useEffect(()=>{
	(async ()=>{
		  try {
			const res = await fetch('./collection.json')
			 .then(elem => elem.json())
			console.log(typeof(res))
			setCards(res)
			
		  } catch (error) {
			console.log(error)
		  }
	})()
},[])
useEffect(()=>{
    localStorage.setItem('cards', JSON.stringify(cards))
    localStorage.setItem('carts', JSON.stringify(carts))
    localStorage.setItem('favorite', JSON.stringify(favorite))
  }, [carts, favorite, cards])

		
console.log(carts);
	const addCarts = (name) => {
		const index = carts.findIndex(({name: arrName}) => {
		  return name === arrName
		})
		console.log(index);
		if (index === -1) {
		  setCarts((current) => [...current,  {name, count:1}])
		}
		 else
		  setCarts((current) => {
			const newState = [...current]
			newState[index].count = newState[index].count+1
			return newState
		  })
	  };
	  
	  const deleteCarts = (name) => {
		const index = carts.findIndex(({name: arrName}) => {
			return name === arrName
		  })
		  const newState = [...carts]
		  console.log(index);
		  if (newState[index].count === 1) {
			newState.splice(index, 1)
		  } else {
			newState[index].count = newState[index].count - 1
		  }
		  setCarts((current) => newState)
	  }
	  	
	  const addFavorite = (name) => {
		const index = cards.findIndex(({name: arrayName}) => {
			return name === arrayName;
		  })
		  const newStateCards = [...cards]
		  console.log(newStateCards.isFavorite);
		  if (newStateCards[index].isFavorite === false) {
			newStateCards[index].isFavorite = true
			setCards(() => newStateCards)
			setFavorite((current)=> [...current, {name}] )}
		else{if(newStateCards[index]){
			newStateCards[index].isFavorite = false
			  favorite.splice(index)
			  setCards(()=>newStateCards)
			}}
  };
console.log( favorite);
  const delFavorite = (name) => {
	const index = favorite.findIndex(({name: arrayName}) => {
		return name === arrayName;
	  })
	  const newState = [...favorite]
	  console.log(newState);
	  newState.splice(index, 1)
	  setFavorite((current)=>newState)
	
	}

	
		return(
			<Router>
			<div className="App">
				<header>
					<Header/>
					<Cart carts={carts} current={current}/>
				</header>
				<main>
				<Modal isAddModal={isAddModal} closeModal={closeModal} modal = {modal} deleteCarts={deleteCarts} addCarts={addCarts} carts={carts} current={current}/>
				<Routes setIsAddModal={setIsAddModal} delFavorite={delFavorite} favorite={favorite} openModal={openModal} modal = {modal}  cards = {cards} addCarts={addCarts} current={current} addFavorite={addFavorite} carts={carts} deleteCarts={deleteCarts}/>
				</main>
			</div>
			</Router>
		);
	
}
export default App;