import React from "react";
import { Formik, Field, ErrorMessage} from "formik";
import * as yup from 'yup'
import s from './Form.module.scss'
import {useDispatch} from "react-redux";
import {cleanerCart} from "../../AppStore/actionsCreator/actionsCreator"
import CustomInput from './CastomInput'

const ERROR_MESSAGE = 'Не правильно заполнено!!'

function Forms(props) {
  const {carts} = props
  console.log(carts);
  const initialValues = {
    name: '',
    secondName: '',
    age: '',
    address: '',
    phone: '',
  }
  const dispatch = useDispatch()

  const handleSubmit = (values) => {
    const current = carts.map(({name}) => name)
    dispatch(cleanerCart([]))
    console.log(values, current)
    localStorage.removeItem('carts')
  }


  const validation = yup.object().shape({
    name: yup.string().required(ERROR_MESSAGE),
    secondName: yup.string().required(ERROR_MESSAGE),
    age: yup.string().required(ERROR_MESSAGE),
    address: yup.string().required(ERROR_MESSAGE),
    phone: yup.number().required(ERROR_MESSAGE).min(10, 'Ввести номер телефона'),
  })
  return (
   <Formik
    initialValues={initialValues}
    validation={validation}
    onSubmit={handleSubmit}
   >
     {(props) => {
       const {values} = props
       return (
        <div className={s.form}>
          <p>
            <label className={s.label} htmlFor='name'>Имя: </label>
            <Field
             type="text"
             name="name"
             value={values.name}
             component={CustomInput}
             error={props.errors.name}
            />
            <ErrorMessage name={'name'} render={(ERROR_MESSAGE) => <span className={s.error}>{ERROR_MESSAGE}</span>}/>
          </p>
          <p>
            <label className={s.label} htmlFor='name'>Фамилия: </label>
            <Field
             type="text"
             name="secondName"
             component={CustomInput}
             value={values.secondName}
             error={props.errors.name}
            />
            <ErrorMessage name={'secondName'} render={(ERROR_MESSAGE) => <span className={s.error}>{ERROR_MESSAGE}</span>}/>
          </p>
          <p>
            <label className={s.label} htmlFor='name'>Возраст: </label>
            <Field
             type="text"
             name="age"
             value={values.age}
             component={CustomInput}
             error={props.errors.name}
            />
            <ErrorMessage name={'age'} render={(ERROR_MESSAGE) => <span className={s.error}>{ERROR_MESSAGE}</span>}/>
          </p>
          <p>
            <label className={s.label} htmlFor='name'>Адрес: </label>
            <Field
             type="text"
             name="address"
             value={values.address}
             component={CustomInput}
             error={props.errors.name}
            />
            <ErrorMessage name={'address'} render={(ERROR_MESSAGE) => <span className={s.error}>{ERROR_MESSAGE}</span>}/>
          </p>
          <p>
            <label className={s.label} htmlFor='name'>Телефон: </label>
            <Field
             type="text"
             name="phone"
             value={values.phone}
             component={CustomInput}
             error={props.errors.name}
            />
            <ErrorMessage name={'phone'} render={(ERROR_MESSAGE) => <span className={s.error}>{ERROR_MESSAGE}</span>}/>
          </p>
          <button
           className={s.submit}
           onClick={()=> { handleSubmit(values)}}
           type="submit">
            Отправить
          </button>
        </div>
       )

     }}
   </Formik>
  )
}

export default Forms