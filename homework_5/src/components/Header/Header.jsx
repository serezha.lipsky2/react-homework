import React from "react";
import s from './Header.module.scss'
import { Link } from "react-router-dom";


function Header () {

	
		return(
			
			<header className = {s.header}>
				<div className = {s.container}>
					<div className= {s.logo}>
						<h2 className = {s.logoName}>Comp-<span>Tech</span></h2>
					</div>
					<nav className={s.pageNav}>
						<ul className={s.pageLinks}>
							<li><Link className={s.links} to="/">Домашняя</Link></li>
							<li><Link className={s.links} to="/cart">Корзина</Link></li>
							<li><Link className={s.links} to="/favorite">Избранное</Link></li>
						</ul>
					</nav>
				</div>
			</header>
			
		)
	
}

export default Header;